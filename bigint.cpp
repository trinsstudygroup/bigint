#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

string reverse(string);
string inputnums();
void pad(string&, string&, int&, int&);
string add(string, string, int);
void print(string, string, string, int, int);

int main (){
    int size1, size2, orgsize1, orgsize2;
    string num1 = "", num2 = "", onereverse = "", tworeverse = "", totalreverse = "";
    string reverse_answer = "", answer = "";

    num1 = inputnums();
    num2 = inputnums();
    onereverse = reverse(num1);
    size1 = onereverse.length();
    orgsize1 = size1;
    tworeverse = reverse(num2);
    size2 = tworeverse.length();
    orgsize2 = size2;

    pad(onereverse, tworeverse, size1, size2);

    reverse_answer = add(onereverse, tworeverse, size1);
    answer = reverse(reverse_answer);
    cout << "The total reverse : " << reverse_answer << endl;
    cout << "The answer is : " << answer << endl;

    print(num1, num2, answer, orgsize1, orgsize2);
    return 0;
}

string inputnums() {

    int size;
    string one = "";

    cout << "Please enter a number: ";
    cin >> one;
    size = one.length();

    while (size > 20) {
        cout << "Error: The number you entered has more then 20 digits," << endl;
        cout << "Please re-enter a new number: ";
        cin >> one;
        size = one.length();
    }
    for (int i = 0; i < size; i++){
        if (isdigit(one[i])){
            i++;
        }
        else {
            cout << "Error: A character was detected in your number please re-enter a new number: ";
            i = 0;
            cin >> one;
            size = one.length();
            while (size > 20) {
                cout << "Error: The number you entered has more then 20 digits," << endl;
                cout << "Please re-enter a new number: ";
                cin >> one;
                size = one.length();
            }
        }
    }
    return one;
}

string reverse(string bigint){

    string a = bigint, b = "";

    b = string(a.rbegin(),a.rend());

    return b;
}

void pad (string& num1, string& num2, int& sizeofnum1, int& sizeofnum2){
    if (sizeofnum1 > sizeofnum2){
        for (int i = sizeofnum2; i < sizeofnum1; i++){
            num2 += '0';
        }
        sizeofnum2 = num2.length();
    }
    else if (sizeofnum2 > sizeofnum1){
        for (int i = sizeofnum1; i < sizeofnum2; i++){
            num1 += '0';
        }
        sizeofnum1 = num1.length();
    }
}

string add (string num1, string num2, int size){

    int carry = 0;
    string answer = "";

    cout << num1 <<','<< num2 << endl;


    for (int i = 0; i < size; i++){

        int temp1 = num1[i] - '0';
        int temp2 = num2[i] - '0';
        int sum = temp1 + temp2 + carry;
        carry = 0;
        if (sum > 9){
            carry = 1;
            sum -= 10;
        }
        answer += sum + '0';
    }
    if (carry == 1){
        answer += '1';
    }
    return answer;
}

void print(string num1, string num2, string answer, int size1, int size2){
    if (size1 < size2) {
        cout << setw(21) << num2 << endl;
        cout << "+" << setw(20) << num1 << endl;
        cout << "----------------------" << endl;
        cout << setw(21) << answer << endl;
    }
    else {
        cout << setw(21) << num1 << endl;
        cout << "+" << setw(20) << num2 << endl;
        cout << "_____________________" << endl;
        cout << setw(21) << answer << endl;
    }
}